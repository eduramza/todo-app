package com.eduramza.todoapp_coroutinestest.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.eduramza.todoapp_coroutinestest.R
import com.eduramza.todoapp_coroutinestest.Todo
import com.eduramza.todoapp_coroutinestest.ViewModelFactory
import com.eduramza.todoapp_coroutinestest.database.Repository
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }


    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: TodoAdapter
    private lateinit var repository : Repository

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        repository = Repository(context!!)
        val factory = ViewModelFactory(repository)
        viewModel = ViewModelProvider(this, factory).get(MainViewModel::class.java)

        setupList()
        observer()
        fab_add.setOnClickListener { viewModel.insertNewTask() }
    }

    private fun setupList(){
        adapter = TodoAdapter(mutableListOf())
        rv_todo_list.layoutManager = LinearLayoutManager(context)
        rv_todo_list.adapter = adapter
    }

    private fun observer(){
        viewModel.insertNewTask()

        viewModel.getTodos().observe(viewLifecycleOwner, Observer{
            adapter.updateList(it as MutableList<Todo>)
        })

    }

}

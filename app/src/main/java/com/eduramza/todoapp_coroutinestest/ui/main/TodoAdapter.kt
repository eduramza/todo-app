package com.eduramza.todoapp_coroutinestest.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.eduramza.todoapp_coroutinestest.R
import com.eduramza.todoapp_coroutinestest.Todo
import kotlinx.android.synthetic.main.item_todo_list.view.*

class TodoAdapter(
    private val itemList: MutableList<Todo>
) : RecyclerView.Adapter<TodoAdapter.ViewHolder>() {

    override fun getItemCount() = itemList.size

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_todo_list))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindViewHolder(itemList[position])
    }

    fun updateList(itemList: MutableList<Todo>) {
        this.itemList.clear()
        this.itemList.addAll(itemList)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var item: Todo

        fun bindViewHolder(item: Todo) {
            this.item = item

            itemView.tv_task.text = item.text
        }
    }
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}
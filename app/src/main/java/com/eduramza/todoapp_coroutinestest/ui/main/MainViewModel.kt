package com.eduramza.todoapp_coroutinestest.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.eduramza.todoapp_coroutinestest.Todo
import com.eduramza.todoapp_coroutinestest.database.Repository
import kotlinx.coroutines.launch

class MainViewModel(private val repository: Repository) : ViewModel() {

    private val _todos = MutableLiveData<List<Todo>>()
    fun getTodos() = _todos

    fun insertNewTask(){
        viewModelScope.launch {
            val newList = repository.insertTask("My task")
            _todos.value = newList
        }
    }

}

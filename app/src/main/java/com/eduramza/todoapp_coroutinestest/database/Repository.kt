package com.eduramza.todoapp_coroutinestest.database

import android.content.Context
import com.eduramza.todoapp_coroutinestest.Todo
import kotlinx.coroutines.*

class Repository(context: Context) {

    val db = AppDatabase.invoke(context)

    suspend fun insertTask(text: String): List<Todo>  {
        var result : List<Todo> = emptyList()
        coroutineScope {
            launch {
                val todo = Todo(text = text)
                db.myDao().addItem(todo)
                result =  db.myDao().getItems()
            }
        }
        return result
    }

}
package com.eduramza.todoapp_coroutinestest.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.eduramza.todoapp_coroutinestest.Todo

@Dao
interface TodoDao {

    /**
     * Main Safe, Custom Dispatcher, Cancellable
     */
    @Insert
    suspend fun addItem(todo: Todo)

    @Query("SELECT * FROM Todo")
    suspend fun getItems(): List<Todo>

    @Query("UPDATE Todo SET completed = 1 WHERE id = :id")
    suspend fun markCompleted(id: Int)
}